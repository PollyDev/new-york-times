package com.fx.newyorktimes.di.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContextQualifier