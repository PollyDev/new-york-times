package com.fx.newyorktimes.di.modules

import android.content.Context
import com.fx.newyorktimes.di.qualifiers.ApplicationContextQualifier
import com.fx.newyorktimes.di.scopes.CustomApplicationScope
import dagger.Module
import dagger.Provides

@Module
class ApplicationContextModule(private var context: Context) {

    @Provides
    @CustomApplicationScope
    @ApplicationContextQualifier
    fun getContext() = context
}