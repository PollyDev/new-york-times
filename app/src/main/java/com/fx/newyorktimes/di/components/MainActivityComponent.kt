package com.fx.newyorktimes.di.components

import com.fx.newyorktimes.activities.MainActivity
import com.fx.newyorktimes.di.modules.MainActivityModule
import com.fx.newyorktimes.di.scopes.MainActivityScope
import com.fx.newyorktimes.viewModel.MovieViewModel
import dagger.Component

@MainActivityScope
@Component(modules = [MainActivityModule::class], dependencies = [AppComponent::class])
interface MainActivityComponent : AppComponent {
    fun inject(mainActivity: MainActivity)

    fun viewModel(): MovieViewModel
}