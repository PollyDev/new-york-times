package com.fx.newyorktimes.di.modules

import androidx.lifecycle.ViewModelProviders
import com.fx.newyorktimes.activities.MainActivity
import com.fx.newyorktimes.activities.MovieDataSource
import com.fx.newyorktimes.backend.ApiService
import com.fx.newyorktimes.di.scopes.MainActivityScope
import com.fx.newyorktimes.util.ViewModelFactory
import com.fx.newyorktimes.viewModel.MovieViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Scope

@Module
class MainActivityModule constructor(private val mainActivity: MainActivity) {
    @Provides
    @MainActivityScope
    fun movieDataSource(apiService: ApiService) = MovieDataSource(apiService)

    @Provides
    @MainActivityScope
    fun mainActivityViewModelFactory(movieDataSource: MovieDataSource) = ViewModelFactory(movieDataSource)

    @Provides
    @MainActivityScope
    fun mainActivityViewModel(viewModelFactory: ViewModelFactory): MovieViewModel =
            ViewModelProviders.of(mainActivity, viewModelFactory).get(MovieViewModel::class.java)
}