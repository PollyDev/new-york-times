package com.fx.newyorktimes.di.components

import com.fx.newyorktimes.backend.ApiService
import com.fx.newyorktimes.di.modules.PicassoModule
import com.fx.newyorktimes.di.modules.ServiceUtilModule
import com.fx.newyorktimes.di.scopes.CustomApplicationScope
import com.squareup.picasso.Picasso
import dagger.Component

@CustomApplicationScope
@Component(modules = [PicassoModule::class, ServiceUtilModule::class])
interface AppComponent {

    fun getPicasso(): Picasso

    fun getApiService(): ApiService
}