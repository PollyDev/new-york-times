package com.fx.newyorktimes.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fx.newyorktimes.activities.MovieDataSource
import com.fx.newyorktimes.response.MovieResponse
import com.fx.newyorktimes.response.Result
import com.fx.newyorktimes.util.NonNullMediatorLiveData
import kotlinx.coroutines.*


class MovieViewModel constructor(private val movieDataSource: MovieDataSource): ViewModel() {

    private val _movies = NonNullMediatorLiveData<List<MovieResponse.ResultResponse>>()
    private val _error = NonNullMediatorLiveData<String>()

    val movies : LiveData<List<MovieResponse.ResultResponse>>get() = _movies
    val errors: LiveData<String>get() = _error

    init {
        initGetMoviesCall()
    }

    private fun initGetMoviesCall() {
        GlobalScope.launch {
            val result = movieDataSource.getMovies("marvel")
            when(result) {
                is Result.Success -> _movies.postValue(result.data.results)
                is Result.Error -> _error.postValue(result.exception.message)
            }
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        GlobalScope.cancel()
    }
}