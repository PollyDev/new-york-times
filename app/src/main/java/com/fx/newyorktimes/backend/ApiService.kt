package com.fx.newyorktimes.backend


import com.fx.newyorktimes.response.MovieResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response

import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    /*The Deferred usually used with await suspending function on order to wait for
    the result without blocking the main/current thread
    Function returns A deferred value rather than Response*/

    @GET(value = "search.json")
    fun search(
        @Query(value = "query") query: String,
        @Query(value = "api-key") apiKey: String
    ): Deferred<Response<MovieResponse>>
}