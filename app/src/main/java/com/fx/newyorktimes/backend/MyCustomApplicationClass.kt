package com.fx.newyorktimes.backend

import android.app.Application
import com.fx.newyorktimes.di.components.AppComponent
import com.fx.newyorktimes.di.components.DaggerAppComponent
import com.fx.newyorktimes.di.modules.ApplicationContextModule
import timber.log.Timber

class MyCustomApplicationClass : Application() {

    private lateinit var applicationComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        applicationComponent = DaggerAppComponent
            .builder()
            .applicationContextModule(ApplicationContextModule(this))
            .build()

    }

    fun getAppComponent() = applicationComponent
}