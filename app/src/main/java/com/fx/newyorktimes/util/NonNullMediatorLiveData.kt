package com.fx.newyorktimes.util

import androidx.lifecycle.MediatorLiveData

/*The purpose of extending from MediatorLiveData to make of lot of easier to use,
especially making it NotNull safe*/

class NonNullMediatorLiveData<T> : MediatorLiveData<T>()
