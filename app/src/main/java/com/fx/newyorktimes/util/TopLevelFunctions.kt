package com.fx.newyorktimes.util

import com.fx.newyorktimes.response.Result
import java.io.IOException
import java.lang.Exception

/*For do not of adding the try/catch to every Network request
created safeApiCall to just trigger the request*/

suspend fun <T: Any> safeApiCall(call: suspend () -> Result<T>, errorMessage: String): Result<T> = try {
    call.invoke()
} catch (e: Exception) {
    Result.Error(IOException(errorMessage, e))
}

val <T> T.exhaustive: T get() = this