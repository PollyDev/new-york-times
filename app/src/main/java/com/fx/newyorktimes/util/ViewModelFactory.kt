package com.fx.newyorktimes.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fx.newyorktimes.activities.MovieDataSource
import com.fx.newyorktimes.viewModel.MovieViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory constructor(private val moviewDataSource: MovieDataSource):
ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>) = MovieViewModel(moviewDataSource) as T

}
