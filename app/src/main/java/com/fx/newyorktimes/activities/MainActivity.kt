package com.fx.newyorktimes.activities

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import com.fx.newyorktimes.R
import com.fx.newyorktimes.di.components.DaggerAppComponent
import com.fx.newyorktimes.di.components.DaggerMainActivityComponent
import com.fx.newyorktimes.di.modules.MainActivityModule
import com.fx.newyorktimes.response.MovieResponse
import com.fx.newyorktimes.util.nonNull
import com.fx.newyorktimes.util.observe
import com.fx.newyorktimes.viewModel.MovieViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), LifecycleOwner {

    private val movies = mutableListOf<MovieResponse.ResultResponse>()

    @Inject
    lateinit var viewModel: MovieViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerMainActivityComponent.builder()
            .mainActivityModule(MainActivityModule(this))
            .appComponent(getAppComponent())
            .build()
            .inject(this)

        startListeningMovies()
        listenToErrors()
    }

    private fun startListeningMovies() {
        viewModel
            .movies
            .nonNull()
            .observe(this) {
                this.movies.addAll(it)
                Toast.makeText(this, it.size.toString(), Toast.LENGTH_SHORT).show()
            }
    }

    private fun listenToErrors() {
        viewModel.errors
            .nonNull()
            .observe(this) {
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            }
    }

}
