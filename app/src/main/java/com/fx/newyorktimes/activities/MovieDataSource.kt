package com.fx.newyorktimes.activities

import com.fx.newyorktimes.backend.ApiService
import com.fx.newyorktimes.response.MovieResponse
import com.fx.newyorktimes.response.Result
import com.fx.newyorktimes.util.safeApiCall
import java.io.IOException

class MovieDataSource constructor(private val apiService: ApiService) {

    companion object {
        private const val API_KEY = "qKliAVFQ2O1F8nQCCU73fex9TOBzZv0d"
    }

    suspend fun getMovies(searchQuery: String) = safeApiCall(
        call = { fetchMovies(searchQuery)},
        errorMessage = "Error occurred"
    )

    suspend fun fetchMovies(searchQuery: String): Result<MovieResponse> {
        val response = apiService.search(searchQuery, API_KEY).await()
        if(response.isSuccessful) {
            return Result.Success(response.body()!!)
        }
        return Result.Error(IOException("Error occurred during fetching movies"))
    }
}