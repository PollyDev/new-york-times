package com.fx.newyorktimes.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import com.fx.newyorktimes.backend.MyCustomApplicationClass

@SuppressLint("Registered")
open class BaseActivity: AppCompatActivity(){
    protected fun getAppComponent() = getApp().getAppComponent()

    private fun getApp() = applicationContext as MyCustomApplicationClass
}