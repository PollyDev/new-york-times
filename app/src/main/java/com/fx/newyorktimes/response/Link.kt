package com.fx.newyorktimes.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Link(
  @SerializedName("type")
  @Expose
  val type: String,
  @SerializedName("url")
  @Expose
  val url: String,
  @SerializedName("suggested_link_text")
  @Expose
  val suggestedLinkText: String
)