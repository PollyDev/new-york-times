package com.fx.newyorktimes.response

import java.lang.Exception

//Sealed classes to wrap-up the response in Success and in the Error case.

sealed class Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
}