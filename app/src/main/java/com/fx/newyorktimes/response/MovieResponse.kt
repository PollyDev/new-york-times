package com.fx.newyorktimes.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MovieResponse(
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("copyright")
    @Expose
    val copyright: String,
    @SerializedName("has_more")
    @Expose
    val hasMore: Boolean,
    @SerializedName("num_results")
    @Expose
    val numResults: Int,
    @SerializedName("results")
    @Expose
    var results: ArrayList<ResultResponse>
) {

    data class ResultResponse(
        @SerializedName("display_title")
        @Expose
        val title: String,
        @SerializedName("mpaa_rating")
        @Expose
        val mppaRating: String,
        @SerializedName("critics_pick")
        @Expose
        val criticsPick: Int,
        @SerializedName("byline")
        @Expose
        val byline: String,
        @SerializedName("headline")
        @Expose
        val headline: String,
        @SerializedName("summary_short")
        @Expose
        val summaryShort: String,
        @SerializedName("publication_date")
        @Expose
        val publicationDate: String,
        @SerializedName("opening_date")
        @Expose
        val openingDate: String,
        @SerializedName("date_updated")
        @Expose
        val dateUpdated: String,
        @SerializedName("link")
        @Expose
        val link: Link,
        @SerializedName("multimedia")
        @Expose
        val multimedia: Multimedia
    )
}


